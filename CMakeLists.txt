cmake_minimum_required(VERSION 3.17)

project(GUI-Sample)

file(GLOB_RECURSE SRC
	"dependencies/sqrat/include/*.h"
	"dependencies/AST/*.h"
	"dependencies/AST/*.cpp"

	"src/sqmodule_api.h"
	"src/sqmodule_api.cpp"
	"src/pch.h"
	"src/sqmain.cpp"
)

if (NOT TARGET squirrel_static)
	add_subdirectory("dependencies/squirrel/squirrel")
	target_include_directories(squirrel_static PRIVATE "dependencies/squirrel/include/")
endif()


if (NOT TARGET GUI)
	add_subdirectory("dependencies/GUI")
endif()

add_library(GUI-Sample SHARED ${SRC})
target_precompile_headers(GUI-Sample PRIVATE "src/pch.h")

target_include_directories(GUI-Sample
	PRIVATE
		"dependencies/AST/"
		"dependencies/squirrel/include/"
		"dependencies/sqrat/include/"
)

# definining __DEBUG and __RELEASE macros depending of configuration type
target_compile_definitions(GUI-Sample PUBLIC $<$<CONFIG:Debug>:__DEBUG>$<$<CONFIG:RelWithDebInfo>:__RELEASE>)

target_link_libraries(GUI-Sample 
	PRIVATE 
		squirrel_static
		GUI
)

if (EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/.game.dir.txt")
	file(READ ".game.dir.txt" GAME_DIR)

	add_custom_command(TARGET GUI-Sample 
		POST_BUILD
			COMMAND ${CMAKE_COMMAND} -E copy $<TARGET_FILE:GUI-Sample> ${GAME_DIR}
			COMMAND ${CMAKE_COMMAND} -E echo "[Post Build] $<TARGET_FILE_NAME:GUI-Sample> copied to: ${GAME_DIR}")
endif()

if (EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/.server.dir.txt")
	file(READ ".server.dir.txt" SERVER_DIR)

	add_custom_command(TARGET GUI-Sample 
		POST_BUILD
			COMMAND ${CMAKE_COMMAND} -E copy $<TARGET_FILE:GUI-Sample> ${SERVER_DIR}
			COMMAND ${CMAKE_COMMAND} -E echo "[Post Build] $<TARGET_FILE_NAME:GUI-Sample> copied to: ${SERVER_DIR}")
endif()

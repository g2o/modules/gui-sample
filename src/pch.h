#ifdef __DEBUG
#include <iostream>
#define DEBUG_PRINT(msg) std::cout << msg;
#endif

#ifdef __RELEASE
#define DEBUG_PRINT(msg)
#endif

#define QUOTIFY(arg) #arg
#define STRINGIFY(arg) QUOTIFY(arg)

#include "AST.h"
#undef Yield // winbase.h
#undef DrawLine // AST/Utils.h

#define SCRAT_EXPORT // Used to get SQRAT_API macro for sqmodule_load func (export function)

#include "sqmodule_api.h"
#include "sqrat.h"

#include <vector> // CDraw3d